#!/bin/bash

VERSION_DEPLOY="TEM-mov-2019-v05-r23-b19.tar.bz2"
PASSWORD_USER="I4Ow9PoE6gmBwna6"
USER="vass"
HOSTNAME="magento-nodo1-produccion.novalocal"
MYSQL_USER="magento"
MYSQL_PASSWORD="FTS@MTG0432gnP"
MYSQL_HOST="10.0.1.91"
DB="magento_db"

if [ ${HOSTNAME} = "magento-nodo1-produccion.novalocal" ]
	then
		tar -czvf /opt/backups/magento-backups/html$(date '+%Y-%m-%d').tar.gz /var/www/html/
		mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${DB} | gzip > /opt/backups/db-magento-backups/${DB}_$(date +%F_%T).sql.gz
		echo "Backups listos"
fi


curl -u ${USER}:${PASSWORD_USER} https://porotos.qai.entrepids.com/dist/movistar/${VERSION_DEPLOY}  -o ${VERSION_DEPLOY} 

if [ $? -eq 0 ]
	then
		mv ${VERSION_DEPLOY} /var/www/html/
		cd /var/www/html/
		tar -xvf ${VERSION_DEPLOY}
    	chown -R magento:nginx /var/www/html/
    	echo "Version lista"
    else 
    	echo "Descarga fallo"
    	exit 0	
fi 

if [ ${HOSTNAME} = "magento-nodo1-produccion.novalocal" ]
	then
   		sudo -u nginx php bin/magento setup:upgrade --keep-generated	
		sudo -u nginx php bin/magento cache:flush

fi