#!/bin/bash

usrs=$(ls /shared)

# comprobamos si existe el usuario PARTE 1
for usr in ${usrs[@]}
do
        uname=$(cat /etc/passwd | grep $usr)
        if [ $uname ]
        then
             /usr/bin/echo -e "El usuario $usr ya existe"
        else
             /usr/bin/echo -e "Creando el usuario $usr..."
             /usr/sbin/useradd $usr -m
             /usr/bin/echo -e "echo Hola $usr, Bienvenido!" >> /home/$usr/.bashrc
        fi
# agregamos carpetas y se valida usuario PARTE 2
        if [ $uname ]
        then
             /usr/bin/echo -e "No es necesario crear carpetas."
        else
             dirs=$(/usr/bin/cat /shared/$usr)
             for dir in ${dirs[@]}
             do
                    /usr/bin/mkdir /home/$usr/$dir
             done
             if [ $? -eq 0 ]
             then
                     /usr/bin/rm -rf /shared/$usr
             fi
        fi
done
