#!/bin/bash

######################################################################################################
#                                                                                                    #
# Script archivo cvs para metricas grafana                                                           #                                                                                        
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################

access="/var/log/nginx/access.log"
f_date="/var/log/campaign/date.txt"
f_medium="/var/log/campaign/medium.txt"
f_source="/var/log/campaign/source.txt"
f_campaign="/var/log/campaign/campaign.txt"
f_newcampaign="/var/log/campaign/newcampaign.txt"



awk -v d1="$(date --date '-1 min' '+%d/%b/%Y:%T')" '{gsub(/^[\[\t]+/, "", $4);}; $4 > d1' $access > /tmp/campaign.log

if [ $? -eq 0 ] 
	then
        cat $campaign | grep "utm_campaign" | awk '{ print $4 }' >  $f_date
        cat $campaign | grep "utm_campaign" |sed 's/utm_medium=/*/g' | cut -d"*" -f2 | cut -d"&" -f1 >  $f_medium
        cat $campaign | grep "utm_campaign" | sed 's/utm_source=/*/g' | cut -d"*" -f2 | cut -d"&" -f1 > $f_source
        cat $campaign | grep "utm_campaign" | sed 's/utm_campaign=/*/g' | cut -d"*" -f2 | sed 's/&/"/g' | cut -d'"' -f1 > $f_campaign ; cat $f_campaign | awk '{print  $1 }' > $f_newcampaign
        access_final=$(paste -d , $f_date $f_source $f_medium $f_newcampaign > /var/log/campaign/access_final.cvs)
        $access_final
fi

