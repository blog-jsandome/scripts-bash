#!/bin/bash

######################################################################################################
#                                                                                                    #
# Script archivo cvs para metricas grafana                                                           #                                                                                        
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################


ACCESS="/var/log/nginx/access.log"
F_DATE="/var/log/campaign/date.txt"
F_MEDIUM="/var/log/campaign/medium.txt"
F_SOURCE="/var/log/campaign/source.txt"
F_CAMPAIGN="/var/log/campaign/campaign.txt"
F_NEWCAMPAIGN="/var/log/campaign/newcampaign.txt"
ACCESS_FINAL="/var/log/campaign/campaing.log"
USER_DB="nginx"
PASS_DB="Ng1xGr@Fan4"
IP_REMOTE="192.168.0.220"

hora=$(/usr/bin/date --date '-1 min' '+%d/%b/%Y:%H:%M')

/usr/bin/grep $hora $ACCESS >  $ACCESS_FINAL

if [ $? -eq 0 ] && [ -f "$ACCESS_FINAL" ] && [ -s "$ACCESS_FINAL" ]
        then
        /usr/bin/echo -e "Validacion de archivo correcta $(/usr/bin/date '+%d/%b/%Y:%H:%T') " > /var/log/campaing-val.log

        /usr/bin/cat $ACCESS_FINAL | /usr/bin/grep "utm_campaign" | /usr/bin/awk '{ print $4 }' | /usr/bin/sed 's/^.//g' >  $F_DATE
        /usr/bin/cat $ACCESS_FINAL | /usr/bin/grep "utm_campaign" |/usr/bin/sed 's/utm_medium=/*/g' | /usr/bin/cut -d"*" -f2 | /usr/bin/cut -d"&" -f1 >  $F_MEDIUM

        /usr/bin/cat $ACCESS_FINAL | /usr/bin/grep "utm_campaign" | /usr/bin/sed 's/utm_source=/*/g' | /usr/bin/cut -d"*" -f2 | /usr/bin/cut -d"&" -f1 > $F_SOURCE
        /usr/bin/cat $ACCESS_FINAL | /usr/bin/grep "utm_campaign" | /usr/bin/sed 's/utm_campaign=/*/g' | /usr/bin/cut -d"*" -f2 | /usr/bin/sed 's/&/"/g' | /usr/bin/cut -d'"' -f1 > $F_CAMPAIGN ; /usr/bin/cat $F_CAMPAIGN | /usr/bin/awk '{print  $1 }' > $F_NEWCAMPAIGN

        ACCESS_FINAL=$(/usr/bin/paste -d , $F_DATE $F_SOURCE $F_MEDIUM $F_NEWCAMPAIGN > /var/log/campaign/access_final1.csv)
        $ACCESS_FINAL

        RUTA_FINAL="/var/log/campaign/access_final1.csv"
        /usr/bin/sed -i "s/$/,$HOSTNAME/g" $RUTA_FINAL
        /usr/bin/echo -e "Archivo listo.\n" >> /var/log/campaing-val.log

fi
  
/usr/bin/mysql -u${USER_DB} -h${IP_REMOTE} -p${PASS_DB} nginx_metrics -e "load data local infile '${RUTA_FINAL}' into table campaign fields terminated by ',' enclosed by '\"' lines terminated by '\n' (@date, source, medium, campaign_name, hostname) set date=str_to_date(@date, '%d/%b/%Y:%T')"
/usr/bin/echo "La importación fue exitosa" >> /var/log/campaing-val.log