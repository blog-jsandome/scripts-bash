#!/bin/bash
MYSQL_EXPORTER_VERSION="0.12.1"
wget https://github.com/prometheus/mysqld_exporter/releases/download/v${MYSQL_EXPORTER_VERSION}/mysqld_exporter-${MYSQL_EXPORTER_VERSION}.linux-amd64.tar.gz

tar -xzvf mysqld_exporter-${MYSQL_EXPORTER_VERSION}.linux-amd64.tar.gz
cd mysqld_exporter-${MYSQL_EXPORTER_VERSION}.linux-amd64
cp mysqld_exporter /usr/local/bin

# create user
useradd --no-create-home --shell /bin/false mysqld_exporter

chown mysqld_exporter:mysqld_exporter /usr/local/bin/mysqld_exporter

echo '[Unit]
Description=MySQL Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=root
Group=root
Environment="DATA_SOURCE_NAME=exporter:AN7iW%OOuR@(10.0.1.91:3306)/"
Type=simple
ExecStart=/usr/local/bin/mysqld_exporter \
--collect.global_status \
--collect.info_schema.innodb_metrics \
--collect.auto_increment.columns \
--collect.info_schema.processlist \
--collect.binlog_size \
--collect.info_schema.tablestats \
--collect.global_variables \
--collect.info_schema.query_response_time \
--collect.info_schema.userstats \
--collect.info_schema.tables \
--collect.perf_schema.tablelocks \
--collect.perf_schema.file_events \
--collect.perf_schema.eventswaits \
--collect.perf_schema.indexiowaits \
--collect.perf_schema.tableiowaits \
--collect.slave_status

[Install]
WantedBy=multi-user.target'  > /etc/systemd/system/mysql_exporter.service

# enable node_exporter in systemctl
systemctl daemon-reload
systemctl start mysql_exporter
systemctl enable mysql_exporter


echo -e "Setup complete.\n
Add the following lines to /etc/prometheus/prometheus.yml:
\n
  - job_name: 'mysql_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9104']\n

NOTA: Es necesario crear un usuario global en mysql:\n
mysql> GRANT REPLICATION CLIENT, PROCESS ON *.* TO 'exporter'@'localhost' identified by 'abc123';
mysql> GRANT SELECT ON performance_schema.* TO 'exporter'@'localhost';

"

