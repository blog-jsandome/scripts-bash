#!/bin/bash
VARNISH_EXPORTER_VERSION="1.4.1"
wget https://github.com/jonnenauha/prometheus_varnish_exporter/releases/download/${VARNISH_EXPORTER_VERSION}/prometheus_varnish_exporter-${VARNISH_EXPORTER_VERSION}.linux-amd64.tar.gz
tar -xzvf prometheus_varnish_exporter-${VARNISH_EXPORTER_VERSION}.linux-amd64.tar.gz
cd prometheus_varnish_exporter-${VARNISH_EXPORTER_VERSION}.linux-amd64
cp prometheus_varnish_exporter /usr/local/bin

# create user
useradd --no-create-home --shell /bin/false varnish_exporter

chown varnish_exporter:varnish_exporter /usr/local/bin/prometheus_varnish_exporter

echo '[Unit]
Description=Prometheus Varnish Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=varnish_exporter
Group=varnish_exporter
Type=simple
ExecStart=/usr/local/bin/prometheus_varnish_exporter -no-exit

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/varnish_exporter.service

# enable node_exporter in systemctl
systemctl daemon-reload
systemctl start varnish_exporter
systemctl enable varnish_exporter


echo "Setup complete.
By default the exporter listens on port 9131. 
See prometheus_varnish_exporter -h for available options. 
It is recommended to use -no-exit in production to not exit the process on failed scrapes. 
Note that if Varnish is not running, varnishstat will still produce a successful scrape.
"


