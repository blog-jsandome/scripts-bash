#!/usr/bin/env bash

LIFERAY4=$(/usr/bin/curl -m 20 localhost:8080 &>/dev/null; echo $?)
LIFERAY1=$(/usr/bin/curl -m 20 172.31.0.17:8080 &>/dev/null; echo $?)
LIFERAY2=$(/usr/bin/curl -m 20 172.31.0.8:8080 &>/dev/null; echo $?)
LIFERAY3=$(/usr/bin/curl -m 20 172.31.0.113:8080 &>/dev/null; echo $?)
LIFERAYL=$(/usr/bin/curl localhost:8080 --silent | wc -l)

/usr/bin/echo "Liferay1 : $LIFERAY1" > /var/log/liferay_fail.log
/usr/bin/echo "Liferay2 : $LIFERAY2" >> /var/log/liferay_fail.log
/usr/bin/echo "Liferay3 : $LIFERAY3" >> /var/log/liferay_fail.log
/usr/bin/echo "Liferay4 : $LIFERAY4" >> /var/log/liferay_fail.log

if [ "$LIFERAY4" -eq "0" -a "$LIFERAYL" -gt 0 ]; then
    /usr/bin/date
    /usr/bin/echo "$HOSTNAME : Correcto"
    exit 0
fi

JAVA_PID=$(/usr/bin/ps -eo pid,lstart,cmd | /usr/bin/grep -i java | /usr/bin/grep -v grep | /usr/bin/awk '{print $5}')

if [ -n "$JAVA_PID" ]; then
    HORA=$(/usr/bin/date +%T)
    INICIO=$(/usr/bin/date -d"$JAVA_PID" +%s)
    FINAL=$(/usr/bin/date -d"$HORA" +%s)
    TIEMPO=$(( $FINAL-$INICIO ))
    if [ "$TIEMPO" -lt "600" ]; then
       /usr/bin/echo "Liferay se esta recuperando. $TIEMPO Segundos desde el reinicio"
       exit 0
    fi
fi

if [ "$LIFERAY4" -gt "0" -o -z "$JAVA_PID" -o $LIFERAYL -eq 0 ]; then
    MUERTE=$(/usr/bin/date)
    /usr/bin/echo "$MUERTE - $HOSTNAME : Dead"
    /usr/bin/echo "Reiniciando Liferay"
    /usr/bin/kill -9 $(/usr/sbin/pidof java) &>/dev/null
    /usr/bin/sync; echo 3 > /proc/sys/vm/drop_caches
    /usr/bin/systemctl start tomcat
    #/usr/local/bin/mail_reinicio.py
    echo "Mail enviado"
    exit 0
fi

