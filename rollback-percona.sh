#!/bin/bash

######################################################################################################
#                                                                                                    #
# Scriipt para realizar rollback de  percona:                                                        #                                                                                        
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################

USER_DB="devops"
PASS_DB="D3v0ps29@.$"
MASTER_IP="192.168.122.118"
SLAVE_IP="192.168.122.186"
BK_HOME="/opt/backups"
#DATE=$(date +%F)
DATE="2019-08-06"
STATUS_SLAVE=$(mysql -u${USER_DB} -h${SLAVE_IP} -p${PASS_DB} -e " SELECT SERVICE_STATE FROM performance_schema.replication_connection_status;" | grep -c ON )

echo -e "Inicia script..."
# Bajar Slave
if [ "$STATUS_SLAVE" -eq 1 ]; then
	mysql -u${USER_DB} -p${PASS_DB} -h${SLAVE_IP} -e " stop slave";
fi

echo -e "Estatus de esclavo validado."

echo -e "Inicia con rollback dump."

gunzip < ${BK_HOME}/magento_db_${DATE}.sql.gz | mysql -u${USER_DB} -h${MASTER_IP} -p${PASS_DB} magento_db;

echo -e "Rollback ejecutado.\n se procede a reiniciar master y reconfigurar slave.\n"

mysql -u${USER_DB} -h${MASTER_IP} -p${PASS_DB} -e "reset master"
FILE=$(mysql -u${USER_DB} -h${MASTER_IP} -p${PASS_DB} -e "show master status" | awk '{print $1 }'  | cut -f1 | tail -n1)
POS=$(mysql -u${USER_DB} -h${MASTER_IP} -p${PASS_DB} -e "show master status" | awk '{print $2 }'  | cut -f1 | tail -n1)
mysql -u${USER_DB} -p${PASS_DB} -h${SLAVE_IP} -e " CHANGE MASTER TO MASTER_LOG_FILE='${FILE}', MASTER_LOG_POS=${POS}";
mysql -u${USER_DB} -h${SLAVE_IP} -p${PASS_DB} -e "start slave"

echo -e "Ok enviar status completo\n"
mysql -u${USER_DB} -h${SLAVE_IP} -p${PASS_DB} -e "show slave status \G"


