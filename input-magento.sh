#!/bin/bash

read -p "ingresa usuario:"  USER
read -p "ingresa password: " PASSWORD_USER
read -p "Ingresa Version de deploy(nombre de comprimido): " VERSION_DEPLOY
read -p "Ingresa Hostname nodo 1 magento: " HOSTNAME
read -p "Ingresa usuario de base de datos: " MYSQL_USER
read -p "Ingrese password de usuario base de datos: " MYSQL_PASSWORD
read -p "Ingrese ip de base de datos: " MYSQL_HOST
read -p "Ingrese esquema de base de datos: " DB

if [ ${HOSTNAME} = "magento-nodo1-produccion.novalocal" ] 

then 
	
	tar -czvf /opt/backups/magento-backups/html$(date '+%Y-%m-%d').tar.gz /var/www/html/ 
	mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${DB} | gzip > /opt/backups/db-magento-backups/${DB}_$(date +%F_%T).sql.gz
	echo "Backups creados"

 else 

 	echo "error backups existen"

fi 


curl -u ${USER}:${PASSWORD_USER} https://porotos.qai.entrepids.com/dist/movistar/${VERSION_DEPLOY}  -o ${VERSION_DEPLOY} 

 if [ $? -eq 0 ] 
 then 
	mv ${VERSION_DEPLOY} /var/www/html/
	cd /var/www/html/
	tar -xvf ${VERSION_DEPLOY}
    chown -R magento:nginx /var/www/html/
    echo "Version lista"
 else 

 	echo "Descarga fallo"
 	exit 0
 fi
 	
if [ ${HOSTNAME} = "magento-nodo1-produccion.novalocal" ]
	then
   		sudo -u nginx php bin/magento setup:upgrade --keep-generated	
		sudo -u nginx php bin/magento cache:flush

fi