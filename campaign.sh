#!/bin/bash

######################################################################################################
#                                                                                                    #
# Script archivo cvs para metricas grafana                                                           #                                                                                        
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################

file="/Users/jonathansandoval/test-lognginx/access-100.log"
d1=$(date --date="-10 min" "+%b %_d %H:%M")
d2=$(date "+%b %_d %H:%M")

while IFS= read line
do
	[[ $line > $d1 && $line < $d2 || $line=~ $d2 ]] && cat $file | grep "utm_campaign" | awk '{ print $4 }' | sed 's/^.//g' >  fecha.txt
	[[ $line > $d1 && $line < $d2 || $line=~ $d2 ]] && cat $file | grep "utm_campaign" |sed 's/utm_medium=/*/g' | cut -d"*" -f2 | cut -d"&" -f1 >  medium.txt
	[[ $line > $d1 && $line < $d2 || $line=~ $d2 ]] && cat $file | grep "utm_campaign" | sed 's/utm_source=/*/g' | cut -d"*" -f2 | cut -d"&" -f1 > source.txt
	[[ $line > $d1 && $line < $d2 || $line=~ $d2 ]] && cat $file | grep "utm_campaign" | sed 's/utm_campaign=/*/g' | cut -d"*" -f2 | sed 's/&/"/g' | cut -d'"' -f1 > campaign.txt
	campaign="/Users/jonathansandoval/test-lognginx/campaign.txt"
	cat $campaign | awk '{print  $1 }' > newcampaign.txt
	paste -d , fecha.txt source.txt medium.txt newcampaign.txt > access.cvs

done <"$file"

