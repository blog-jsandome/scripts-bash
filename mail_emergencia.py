#!/usr/bin/python

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path

email = 'cloud.vass@gmail.com'
password = '?RW8Y*6;h-h4'
send_to_emails = ['baruch.ramos@vass.com.mx', 'veronica.santos@vass.com.mx', 'jonathan.sandoval@vass.com.mx'] 
subject = 'Emergencia Sitio colapsado!!!'
message = 'Los 3 Nodos no responden!!!!!!!!!'
file_location = '/var/log/test.txt'

filename = os.path.basename(file_location)
attachment = open(file_location, "rb")
part = MIMEBase('application', 'octet-stream')
part.set_payload(attachment.read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(email, password)

for send_to_email in send_to_emails:
    msg = MIMEMultipart()
    msg['From'] = email
    msg['To'] = send_to_email
    msg['Subject'] = subject

    msg.attach(MIMEText(message, 'plain'))
    msg.attach(part)

    server.sendmail(email, send_to_email, msg.as_string()) 

server.quit()
