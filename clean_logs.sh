#!/usr/bin/bash

FECHA=$(/usr/bin/date -d "-1 day" +"%Y-%m-%d")

#Limpiando catalina.out

/usr/bin/date > /opt/liferay/tomcat-8.0.32/logs/catalina.out

/usr/bin/rm -rf /opt/liferay/tomcat-8.0.32/logs/localhost_access_log.$FECHA.txt

/usr/bin/rm -rf /opt/liferay/tomcat-8.0.32/logs/catalina.$FECHA.log


#Limpiando logs Liferay

/usr/bin/rm -rf /opt/liferay/logs/liferay.$FECHA.*
