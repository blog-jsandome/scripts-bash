#!/bin/bash
######################################################################################################
#                                                                                                                                                                                                                                                                                                                #
#        Script para realizar backup de MYSQL:                                                                                                                                                                                                                                                                   #                                                                                        
#                                                                                                                                                                                                                                                                                                                # 
#        Backup todos los dias                                                                                                                                                                                                                                                                                   # 
#                                                                                                                                                                                                                                                                                                                # 
######################################################################################################

BACKUP_MYSQL="true"
MYSQL_USER="root"
MYSQL_PASSWORD="RFK0@slRM24"

WORKING_DIR="/tmp"

/usr/bin/rm -rf $WORKING_DIR/


#Backup de MySQL

if [ "${BACKUP_MYSQL}" = "true" ]
then
        /usr/bin/mkdir -p $WORKING_DIR/percona
        for db in $(mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e 'show databases;' | grep -Ev "^(Database|mysql|information_schema|performance_schema)$")
        do
                mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} "${db}" | gzip > ${WORKING_DIR}/percona/${db}_$(date +%F_%T).sql.gz
        done
        mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} --events --ignore-table=mysql.event --all-databases | gzip > ${WORKING_DIR}/percona/ALL_DATABASES_$(date +%F_%T).sql.gz
fi

/usr/bin/tar cjvf $WORKING_DIR/data-base-$(date +"%F").tar.bz2 -C $WORKING_DIR percona

/usr/bin/mv /tmp/data-base-$(date +"%F").tar.bz2 /var/backups/
#Limpiando
/usr/bin/rm -rf $WORKING_DIR/percona
