#!/bin/bash
######################################################################################################
#                                                                                                    #
# Script para realizar backup de MYSQL:                                                              #                                                                                        
#                                                                                                    # 
# Backup todos los dias                                                                              # 
#                                                                                                    # 
######################################################################################################

BACKUP_MYSQL="true"
MYSQL_USER="root"
MYSQL_PASSWORD="C3rt1Lifer@y"
MYSQL_HOST="10.2.1.193"
WORKING_DIR="/tmp"

/usr/bin/rm -rf $WORKING_DIR/liferay


#Backup de MySQL

if [ "${BACKUP_MYSQL}" = "true" ]
then
        /usr/bin/mkdir -p $WORKING_DIR/liferay
        for db in $(mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -h${MYSQL_HOST} -e 'show databases;' | grep -Ev "^(Database|mysql|information_schema|performance_schema)$")
        do
                mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} "${db}" -h${MYSQL_HOST} | gzip > ${WORKING_DIR}/liferay/${db}_$(date +%F_%T).sql.gz
        done
        mysqldump --opt --routines -u${MYSQL_USER} -p${MYSQL_PASSWORD} -h${MYSQL_HOST} --events --ignore-table=mysql.event --all-databases | gzip > ${WORKING_DIR}/liferay/ALL_DATABASES_$(date +%F_%T).sql.gz
fi

/usr/bin/tar cjvf $WORKING_DIR/data-base-$(date +"%F").tar.bz2 -C $WORKING_DIR liferay

/usr/bin/mv /tmp/data-base-$(date +"%F").tar.bz2 /opt/

#Limpiando
/usr/bin/rm -rf $WORKINGo_DIR/liferay
