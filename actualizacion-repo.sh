#!/bin/bash

######################################################################################################
#                                                                                                    #
# Script archivo actualización de repo                                                           #                                                                                        
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################

DIR1="/var/repo/"
DIR2="/etc/yum.repos.d"
FN=$(find / -name CentOS-7-x86_64-DVD-1908.iso 2>/dev/null)
FN2=$(find / -name Centos-Base-Local.repo 2>/dev/null)

cd $DIR1
mkdir archivos && mkdir isos

mv $FN /var/repo/isos/

echo "/var/repo/isos/CentOS-7-x86_64-DVD-1908.iso  /var/repo/archivos/   iso9660     loop        0 0" >> /etc/fstab

echo "####fstab lista para montar ####"  && cat /etc/fstab

mount -a

echo "#### Particion montada ####" && df -h
mv $FN2 $DIR2

yum update

echo "Actualización completa :) " && cat /etc/red*

