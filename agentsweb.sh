#!/bin/bash

######################################################################################################
#                                                                                                    #
# Script archivo cvs para metricas grafana                                                           #
#                                                                                                    # 
#                                                                                                    # 
#                                                                                                    # 
######################################################################################################


ACCESS_AG="/var/log/nginx/access.log"
ACCESS_AGF="/var/log/agents/useragents.log"
DATE_AG="/var/log/agents/date.txt"
EXPLORADOR="/var/log/agents/explorador.txt"
SISTEMA="/var/log/agents/sistema.txt"
AGENTES="/var/log/agents/agentes.txt"
PASE_FILE="/var/log/agents/pase.txt"
RUTA_FINAL="/var/log/agents/final-agents.csv"
USER_DB="nginx"
PASS_DB="Ng1xGr@Fan4"
IP_REMOTE="192.168.0.220"

hora=$(/usr/bin/date --date '-1 min' '+%d/%b/%Y:%H:%M')

/usr/bin/grep $hora $ACCESS_AG >  $ACCESS_AGF

if [ $? -eq 0 ] && [ -f "$ACCESS_AGF" ] && [ -s "$ACCESS_AGF" ]
        then
        /usr/bin/echo -e "Validacion de archivo correcta $(/usr/bin/date '+%d/%b/%Y:%H:%T') " > /var/log/user_agents.log

        /usr/bin/cat $ACCESS_AGF | /usr/bin/awk '{ print $4 }'| /usr/bin/sed 's/^.//g' > $DATE_AG
        /usr/bin/cat $ACCESS_AGF | /usr/bin/awk '{print $12}' | /usr/bin/sed 's/"//g' > $EXPLORADOR
        /usr/bin/cat $ACCESS_AGF | /usr/bin/sed 's/"/*/g' | /usr/bin/cut -d"*" -f6 | /usr/bin/sed 's/;//g' | /usr/bin/sed s/,//g | /usr/bin/sed 's/(7)/7/g' | /usr/bin/sed 's/(4)/4/g'| /usr/bin/sed 's/(6)/6/g'| /usr/bin/cut -d"(" -f2 | /usr/bin/cut -d")" -f1 | /usr/bin/sed 's/ //g' > $SISTEMA
        /usr/bin/cat $ACCESS_AGF | /usr/bin/sed 's/"/*/g' | /usr/bin/cut -d"*" -f6 | /usr/bin/sed 's/;//g' | /usr/bin/sed s/,//g | /usr/bin/sed 's/(7)/7/g' | /usr/bin/sed 's/(4)/4/g'| /usr/bin/sed 's/(6)/6/g'| /usr/bin/cut -d"(" -f3 | /usr/bin/cut -d")" -f1 > $AGENTES

        /usr/bin/paste -d , $DATE_AG $EXPLORADOR $SISTEMA $AGENTES > $PASE_FILE
        /usr/bin/cat $PASE_FILE | /usr/bin/sed 's/ //g' | /usr/bin/sort u > $RUTA_FINAL
        /usr/bin/echo -e "Archivo listo.\n" >> /var/log/user_agents.log

fi


/usr/bin/mysql -u${USER_DB} -h${IP_REMOTE} -p${PASS_DB} nginx_metrics -e "load data local infile '${RUTA_FINAL}' into table agentes fields terminated by ',' enclosed by '\"' lines terminated by '\n' (@date, explorador, sistema, agente) set date=str_to_date(@date, '%d/%b/%Y:%T')"
/usr/bin/echo "La importación fue exitosa" >> /var/log/user_agents.log
