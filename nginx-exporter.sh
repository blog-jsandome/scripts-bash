#!/bin/bash
NGINX_EXPORTER_VERSION="0.4.1"
#wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v${NGINX_EXPORTER_VERSION}/nginx-prometheus-exporter-${NGINX_EXPORTER}-linux-amd64.tar.gz
wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v0.4.1/nginx-prometheus-exporter-0.4.1-linux-amd64.tar.gz

tar -xzvf nginx-prometheus-exporter-${NGINX_EXPORTER_VERSION}-linux-amd64.tar.gz
cp nginx-prometheus-exporter /usr/local/bin

# create user
useradd --no-create-home --shell /bin/false nginx_exporter

chown nginx_exporter:nginx_exporter /usr/local/bin/nginx-prometheus-exporter

echo '[Unit]
Description=Prometheus Nginx Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=root
Group=root
Type=simple
ExecStart=/usr/local/bin/nginx-prometheus-exporter 

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/nginx_exporter.service

# enable node_exporter in systemctl
systemctl daemon-reload
systemctl start nginx_exporter
systemctl enable nginx_exporter


echo "Setup complete\n.
Configure Prometheus to scrape metrics from the server with the exporter. 
Note that the default scrape port of the exporter is 9113 and the default metrics path -- /metrics.
"


